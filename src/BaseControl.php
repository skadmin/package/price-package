<?php

declare(strict_types=1);

namespace Skadmin\PricePackage;

use App\Model\System\ABaseControl;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;

class BaseControl extends ABaseControl
{
    public const RESOURCE      = 'price-package';
    public const DIR_IMAGE     = 'price-package';
    public const DIR_IMAGE_TAG = 'price-package-tag';

    public const PRIVILEGE_TAGS = 'tags';

    public const ADDITIONAL_PRIVILEGE = [
        self::PRIVILEGE_TAGS,
    ];

    public function getMenu() : ArrayHash
    {
        return ArrayHash::from([
            'control' => $this,
            'icon'    => Html::el('i', ['class' => 'fas fa-fw fa-money-bill-wave']),
            'items'   => ['overview'],
        ]);
    }
}
