<?php

declare(strict_types=1);

namespace Skadmin\PricePackage\Components\Admin;

use App\Components\Form\FormWithUserControl;
use Skadmin\Role\Doctrine\Role\Privilege;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use SkadminUtils\ImageStorage\ImageStorage;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\Arrays;
use Skadmin\PricePackage\BaseControl;
use Skadmin\PricePackage\Doctrine\PricePackage\PricePackage;
use Skadmin\PricePackage\Doctrine\PricePackage\PricePackageFacade;
use Skadmin\PricePackage\Doctrine\PricePackage\PricePackageTag;
use Skadmin\PricePackage\Doctrine\PricePackage\PricePackageTagFacade;
use Skadmin\PricePackage\Doctrine\PricePackage\PricePackageTypeFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\Utils\UtilsFormControl;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;
use function intval;

/**
 * Class Edit
 */
class Edit extends FormWithUserControl
{
    use APackageControl;

    /** @var LoaderFactory */
    public $webLoader;

    /** @var PricePackageFacade */
    private $facade;

    /** @var PricePackageTagFacade */
    private $facadePricePackageTag;

    /** @var PricePackageTypeFacade */
    private $facadePricePackageType;

    /** @var PricePackage */
    private $pricePackage;

    /** @var ImageStorage */
    private $imageStorage;

    public function __construct(?int $id, PricePackageFacade $facade, PricePackageTagFacade $facadePricePackageTag, PricePackageTypeFacade $facadePricePackageType, Translator $translator, LoaderFactory $webLoader, LoggedUser $user, ImageStorage $imageStorage)
    {
        parent::__construct($translator, $user);
        $this->facade = $facade;
        $this->facadePricePackageTag = $facadePricePackageTag;
        $this->facadePricePackageType = $facadePricePackageType;

        $this->webLoader = $webLoader;
        $this->imageStorage = $imageStorage;

        $this->pricePackage = $this->facade->get($id);
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (!$this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    /**
     * @return SimpleTranslation|string
     */
    public function getTitle()
    {
        if ($this->pricePackage->isLoaded()) {
            return new SimpleTranslation('price-package.edit.title - %s', $this->pricePackage->getName());
        }

        return 'price-package.edit.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [
            $this->webLoader->createCssLoader('customFileInput'),
            $this->webLoader->createCssLoader('fancyBox'), // responsive file manager
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            $this->webLoader->createJavaScriptLoader('adminTinyMce'),
            $this->webLoader->createJavaScriptLoader('customFileInput'),
            $this->webLoader->createJavaScriptLoader('fancyBox'), // responsive file manager
        ];
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        $identifier = null;

        // IDENTIFIER
        $identifier = UtilsFormControl::getImagePreview($values->image_preview, BaseControl::DIR_IMAGE);

        if (isset($values->tags)) {
            $tags = Arrays::map($values->tags, function ($tagId): PricePackageTag {
                return $this->facadePricePackageTag->get($tagId);
            });
        }

        $type = $this->facadePricePackageType->get($values->type);

        if ($this->pricePackage->isLoaded()) {
            $pricePackageImagePreview = $this->pricePackage->getImagePreview();
            if ($identifier !== null && $pricePackageImagePreview !== null && trim($pricePackageImagePreview) !== '') {
                $this->imageStorage->delete($this->pricePackage->getImagePreview());
            }

            $pricePackage = $this->facade->update(
                $this->pricePackage->getId(),
                $values->name,
                $values->content,
                intval($values->price),
                $values->is_active,
                $identifier,
                $type,
                $tags ?? $this->pricePackage->getTags()->toArray()
            );
            $this->onFlashmessage('form.price-package.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $pricePackage = $this->facade->create(
                $values->name,
                $values->content,
                intval($values->price),
                $values->is_active,
                $identifier,
                $type,
                $tags ?? []
            );
            $this->onFlashmessage('form.price-package.edit.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'send_back') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render' => 'edit',
            'id' => $pricePackage->getId(),
        ]);
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render' => 'overview',
        ]);
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->imageStorage = $this->imageStorage;
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/edit.latte');

        $template->pricePackage = $this->pricePackage;
        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addText('name', 'form.price-package.edit.name')
            ->setRequired('form.price-package.edit.name.req');
        $form->addText('price', 'form.price-package.edit.price')
            ->setRequired('form.price-package.edit.price.req')
            ->setHtmlType('number')
            ->setHtmlAttribute('min', 0);
        $form->addCheckbox('is_active', 'form.price-package.edit.is-active')
            ->setDefaultValue(true);
        $form->addImageWithRFM('image_preview', 'form.price-package.edit.image-preview');

        // TEXT
        $form->addTextArea('content', 'form.price-package.edit.content');

        if ($this->isAllowed(BaseControl::RESOURCE, BaseControl::PRIVILEGE_TAGS)) {
            $form->addMultiSelect('tags', 'form.price-package.edit.tags', $this->facadePricePackageTag->getPairs('id', 'name'))
                ->setTranslator(null);
        }

        $form->addSelect('type', 'form.price-package.edit.type', $this->facadePricePackageType->getPairs('id', 'name'))
            ->setPrompt(Constant::PROMTP)
            ->setTranslator(null)
            ->setRequired('form.price-package.edit.type.req');

        // BUTTON
        $form->addSubmit('send', 'form.price-package.edit.send');
        $form->addSubmit('send_back', 'form.price-package.edit.send-back');
        $form->addSubmit('back', 'form.price-package.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (!$this->pricePackage->isLoaded()) {
            return [];
        }

        $tagsId = Arrays::map($this->pricePackage->getTags()->toArray(), static function (PricePackageTag $tag): ?int {
            return $tag->getId();
        });

        return [
            'name' => $this->pricePackage->getName(),
            'content' => $this->pricePackage->getContent(),
            'is_active' => $this->pricePackage->isActive(),
            'price' => $this->pricePackage->getPrice(),
            'type' => $this->pricePackage->getType() ? $this->pricePackage->getType()->getId() : null,
            'tags' => $tagsId,
        ];
    }
}
