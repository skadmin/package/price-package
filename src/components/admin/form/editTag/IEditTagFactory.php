<?php

declare(strict_types=1);

namespace Skadmin\PricePackage\Components\Admin;

/**
 * Interface IEditTagFactory
 */
interface IEditTagFactory
{
    public function create(?int $id = null) : EditTag;
}
