<?php

declare(strict_types=1);

namespace Skadmin\PricePackage\Components\Admin;

use App\Components\Form\FormWithUserControl;
use Skadmin\Role\Doctrine\Role\Privilege;
use App\Model\System\APackageControl;
use App\Model\System\Flash;
use SkadminUtils\ImageStorage\ImageStorage;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Skadmin\PricePackage\BaseControl;
use Skadmin\PricePackage\Doctrine\PricePackage\PricePackageTag;
use Skadmin\PricePackage\Doctrine\PricePackage\PricePackageTagFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\Utils\UtilsFormControl;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

/**
 * Class EditTag
 */
class EditTag extends FormWithUserControl
{
    use APackageControl;

    /** @var LoaderFactory */
    private $webLoader;

    /** @var ImageStorage */
    private $imageStorage;

    /** @var PricePackageTagFacade */
    private $facade;

    /** @var PricePackageTag */
    private $pricePackageTag;

    public function __construct(?int $id, PricePackageTagFacade $facade, Translator $translator, LoggedUser $user, LoaderFactory $webLoader, ImageStorage $imageStorage)
    {
        parent::__construct($translator, $user);
        $this->facade       = $facade;
        $this->webLoader    = $webLoader;
        $this->imageStorage = $imageStorage;

        $this->pricePackageTag = $this->facade->get($id);
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, BaseControl::PRIVILEGE_TAGS)) {
            $this->onFlashmessage('grid.price-package.edit-tag.name.flash.info.denide-acccess-tags', Flash::INFO);
            $this->getPresenter()->redirect('Component:default', [
                'package' => new BaseControl(),
                'render'  => 'overview',
            ]);
        }

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    /**
     * @return SimpleTranslation|string
     */
    public function getTitle()
    {
        if ($this->pricePackageTag->isLoaded()) {
            return new SimpleTranslation('price-package.edit-tag.title - %s', $this->pricePackageTag->getName());
        }

        return 'price-package.edit-tag.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss() : array
    {
        return [
            $this->webLoader->createCssLoader('colorPicker'),
            $this->webLoader->createCssLoader('customFileInput'),
            $this->webLoader->createCssLoader('fancyBox'), // responsive file manager
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs() : array
    {
        return [
            $this->webLoader->createJavaScriptLoader('adminTinyMce'),
            $this->webLoader->createJavaScriptLoader('colorPicker'),
            $this->webLoader->createJavaScriptLoader('customFileInput'),
            $this->webLoader->createJavaScriptLoader('fancyBox'), // responsive file manager
        ];
    }

    public function processOnSuccess(Form $form, ArrayHash $values) : void
    {
        // IDENTIFIER
        $identifier = UtilsFormControl::getImagePreview($values->image_preview, BaseControl::DIR_IMAGE_TAG);

        if ($this->pricePackageTag->isLoaded()) {
            if ($identifier !== null && $this->pricePackageTag->getImagePreview() !== null) {
                $this->imageStorage->delete($this->pricePackageTag->getImagePreview());
            }

            $pricePackageTag = $this->facade->update(
                $this->pricePackageTag->getId(),
                $values->name,
                $values->content,
                $values->color,
                $values->is_active,
                $identifier
            );
            $this->onFlashmessage('form.price-package.edit-tag.flash.success.update', Flash::SUCCESS);
        } else {
            $pricePackageTag = $this->facade->create(
                $values->name,
                $values->content,
                $values->color,
                $values->is_active,
                $identifier
            );
            $this->onFlashmessage('form.price-package.edit-tag.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'send_back') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit-tag',
            'id'      => $pricePackageTag->getId(),
        ]);
    }

    public function processOnBack() : void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview-tag',
        ]);
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/editTag.latte');

        $template->pricePackageTag = $this->pricePackageTag;
        $template->render();
    }

    protected function createComponentForm() : Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addText('name', 'form.price-package.edit-tag.name')
            ->setRequired('form.price-package.edit-tag.name.req');
        $form->addTextArea('content', 'form.price-package.edit-tag.content');
        $form->addText('color', 'form.price-package.edit-tag.color');
        $form->addImageWithRFM('image_preview', 'form.price-package.edit-tag.image-preview');

        $form->addCheckbox('is_active', 'form.price-package.edit-tag.is-active')
            ->setDefaultValue(true);

        // BUTTON
        $form->addSubmit('send', 'form.price-package.edit-tag.send');
        $form->addSubmit('send_back', 'form.price-package.edit-tag.send-back');
        $form->addSubmit('back', 'form.price-package.edit-tag.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults() : array
    {
        if (! $this->pricePackageTag->isLoaded()) {
            return [];
        }

        return [
            'name'      => $this->pricePackageTag->getName(),
            'content'   => $this->pricePackageTag->getContent(),
            'color'     => $this->pricePackageTag->getColor(),
            'is_active' => $this->pricePackageTag->isActive(),
        ];
    }
}
