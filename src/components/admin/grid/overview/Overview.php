<?php

declare(strict_types=1);

namespace Skadmin\PricePackage\Components\Admin;

use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use Skadmin\Role\Doctrine\Role\Privilege;
use App\Model\Grid\Traits\IsActive;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use SkadminUtils\ImageStorage\ImageStorage;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Html;
use Skadmin\PricePackage\BaseControl;
use Skadmin\PricePackage\Doctrine\PricePackage\PricePackage;
use Skadmin\PricePackage\Doctrine\PricePackage\PricePackageFacade;
use Skadmin\PricePackage\Doctrine\PricePackage\PricePackageTagFacade;
use Skadmin\PricePackage\Doctrine\PricePackage\PricePackageTypeFacade;
use Skadmin\Translator\Translator;
use SkadminUtils\Utils\Utils\Colors\Colors;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;
use function sprintf;

/**
 * Class Overview
 */
class Overview extends GridControl
{
    use APackageControl;
    use IsActive;

    /** @var PricePackageFacade */
    private $facade;

    /** @var PricePackageTagFacade */
    private $facadePricePackageTag;

    /** @var PricePackageTypeFacade */
    private $facadePricePackageType;

    /** @var LoaderFactory */
    public $webLoader;

    /** @var ImageStorage */
    private $imageStorage;

    public function __construct(PricePackageFacade $facade, PricePackageTagFacade $facadePricePackageTag, PricePackageTypeFacade $facadePricePackageType, Translator $translator, ImageStorage $imageStorage, User $user, LoaderFactory $webLoader)
    {
        parent::__construct($translator, $user);

        $this->facade = $facade;
        $this->facadePricePackageTag = $facadePricePackageTag;
        $this->facadePricePackageType = $facadePricePackageType;
        $this->imageStorage = $imageStorage;
        $this->webLoader = $webLoader;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (!$this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'price-package.overview.title';
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [$this->webLoader->createJavaScriptLoader('jQueryUi')];
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModelForGrid());

        // COLUMNS
        $grid->addColumnText('imagePreview', '')
            ->setRenderer(function (PricePackage $pricePackage): ?Html {
                if ($pricePackage->getImagePreview() !== null) {
                    $imageSrc = $this->imageStorage->fromIdentifier([$pricePackage->getImagePreview(), '60x60', 'exact']);

                    return Html::el('img', [
                        'src' => sprintf('/%s', $imageSrc->createLink()),
                        'style' => 'max-width: none;',
                    ]);
                }

                return null;
            })->setAlign('center');
        $grid->addColumnText('name', 'grid.price-package.overview.name')
            ->setRenderer(function (PricePackage $pricePackage): Html {
                if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
                    $link = $this->getPresenter()->link('Component:default', [
                        'package' => new BaseControl(),
                        'render' => 'edit',
                        'id' => $pricePackage->getId(),
                    ]);

                    $name = Html::el('a', [
                        'href' => $link,
                        'class' => 'font-weight-bold',
                    ]);
                } else {
                    $name = new Html();
                }

                $name->setText($pricePackage->getName());

                return $name;
            });
        $grid->addColumnText('price', 'grid.price-package.overview.price');
        $grid->addColumnText('type', 'grid.price-package.overview.type', 'type.name');
        if ($this->isAllowed(BaseControl::RESOURCE, BaseControl::PRIVILEGE_TAGS)) {
            $grid->addColumnText('tags', 'grid.price-package.overview.tags')
                ->setRenderer(static function (PricePackage $pricePackage): Html {
                    $tags = new Html();
                    foreach ($pricePackage->getTags() as $tag) {
                        $color = $tag->getColor() !== '' ? $tag->getColor() : '#FFFFFF';
                        $tags->addHtml(Html::el('span', [
                            'class' => 'badge mr-1 px-2 mb-1 border border-primary',
                            'style' => sprintf('background-color: %s; color: %s', $color, Colors::getContrastColor($color)),
                        ])->setText($tag->getName()));
                    }
                    return $tags;
                });
        }
        $this->addColumnIsActive($grid, 'price-package.overview');

        // STYLE
        $grid->getColumn('imagePreview')
            ->getElementPrototype('th')
            ->setAttribute('style', 'width: 1px');

        // FILTER
        $grid->addFilterText('name', 'grid.price-package.overview.name');
        $grid->addFilterText('price', 'grid.price-package.overview.price');
        $grid->addFilterSelect('type', 'grid.price-package.overview.type', $this->facadePricePackageType->getPairs('id', 'name'))
            ->setPrompt(Constant::PROMTP);
        if ($this->isAllowed(BaseControl::RESOURCE, BaseControl::PRIVILEGE_TAGS)) {
            $grid->addFilterSelect('tags', 'grid.price-package.overview.tags', $this->facadePricePackageTag->getPairs('id', 'name'), 't.id')
                ->setPrompt(Constant::PROMTP);
        }
        $this->addFilterIsActive($grid, 'price-package.overview');

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addAction('edit', 'grid.price-package.overview.action.edit', 'Component:default', ['id' => 'id'])->addParameters([
                'package' => new BaseControl(),
                'render' => 'edit',
            ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // TOOLBAR
        if ($this->isAllowed(BaseControl::RESOURCE, BaseControl::PRIVILEGE_TAGS)) {
            $grid->addToolbarButton('Component:default#2', 'grid.price-package.overview.action.overview-tag', [
                'package' => new BaseControl(),
                'render' => 'overview-tag',
            ])->setIcon('tags')
                ->setClass('btn btn-xs btn-outline-primary');
        }

        $grid->addToolbarButton('Component:default#3', 'grid.price-package.overview.action.overview-type', [
            'package' => new BaseControl(),
            'render' => 'overview-type',
        ])->setIcon('tags')
            ->setClass('btn btn-xs btn-outline-primary');

        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addToolbarButton('Component:default', 'grid.price-package.overview.action.new', [
                'package' => new BaseControl(),
                'render' => 'edit',
            ])->setIcon('plus')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // SORTING
        $grid->setSortable();
        $grid->setSortableHandler($this->link('sort!'));

        return $grid;
    }

    public function handleSort(?string $item_id, ?string $prev_id, ?string $next_id): void
    {
        $this->facade->sort($item_id, $prev_id, $next_id);

        $presenter = $this->getPresenterIfExists();
        if ($presenter !== null) {
            $presenter->flashMessage('grid.price-package.overview.action.flash.sort.success', Flash::SUCCESS);
        }

        $this['grid']->reload();
    }
}
