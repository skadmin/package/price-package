<?php

declare(strict_types=1);

namespace Skadmin\PricePackage\Components\Admin;

/**
 * Interface IOverviewTypeFactory
 */
interface IOverviewTypeFactory
{
    public function create() : OverviewType;
}
