<?php

declare(strict_types=1);

namespace Skadmin\PricePackage\Components\Admin;

use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use Skadmin\Role\Doctrine\Role\Privilege;
use App\Model\Grid\Traits\IsRequired;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Forms\Container;
use Nette\Security\User;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;
use Skadmin\PricePackage\BaseControl;
use Skadmin\PricePackage\Doctrine\PricePackage\PricePackageType;
use Skadmin\PricePackage\Doctrine\PricePackage\PricePackageTypeFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;
use function boolval;
use function intval;

/**
 * Class Overview
 */
class OverviewType extends GridControl
{
    use APackageControl;
    use IsRequired;

    /** @var PricePackageTypeFacade */
    private $facade;

    /** @var LoaderFactory */
    public $webLoader;

    public function __construct(PricePackageTypeFacade $facade, Translator $translator, User $user, LoaderFactory $webLoader)
    {
        parent::__construct($translator, $user);

        $this->facade = $facade;
        $this->webLoader = $webLoader;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (!$this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overviewType.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'price-package.overview-type.title';
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [$this->webLoader->createJavaScriptLoader('jQueryUi')];
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel());

        // COLUMNS
        $grid->addColumnText('name', 'grid.price-package.overview-type.name');
        $this->addColumnIsRequired($grid, 'price-package.overview');

        // FILTER
        $grid->addFilterText('name', 'grid.price-package.overview-type.name');
        $this->addFilterIsRequired($grid, 'price-package.overview');

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            // INLINE ADD
            $grid->addInlineAdd()
                ->setPositionTop()
                ->setText($this->translator->translate('grid.price-package.overview-type.action.inline-add'))
                ->onControlAdd[] = [$this, 'onInlineAdd'];
            $grid->getInlineAddPure()
                ->onSubmit[] = [$this, 'onInlineAddSubmit'];

            // INLINE EDIT
            $grid->addInlineEdit()
                ->onControlAdd[] = [$this, 'onInlineEdit'];
            $grid->getInlineEditPure()->onSetDefaults[] = [$this, 'onInlineEditDefaults'];
            $grid->getInlineEditPure()->onSubmit[] = [$this, 'onInlineEditSubmit'];
        }


        // TOOLBAR
        $grid->addToolbarButton('Component:default#2', 'grid.price-package-reservatio.overview-type.action.overview', [
            'package' => new BaseControl(),
            'render'  => 'overview',
        ])->setIcon('list-ul')
            ->setClass('btn btn-xs btn-outline-primary');

        return $grid;
    }

    public function onInlineAdd(Container $container): void
    {
        $container->addText('name', 'grid.price-package.overview-type.name')
            ->setRequired('grid.price-package.overview-type.name.req');
        $container->addSelect('isRequired', 'grid.price-package.overview-type.is-required', Constant::DIAL_YES_NO)
            ->setDefaultValue(Constant::NO);
    }

    /**
     * @param ArrayHash<mixed> $values
     */
    public function onInlineAddSubmit(ArrayHash $values): void
    {
        $pricePackageTag = $this->facade->create($values->name, boolval($values->isRequired));

        $message = new SimpleTranslation('grid.price-package.overview-type.action.flash.inline-add.success "%s"', [$pricePackageTag->getName()]);
        $this->onFlashmessage($message, Flash::SUCCESS);
    }

    public function onInlineEdit(Container $container): void
    {
        $container->addText('name', 'grid.price-package.overview-type.name')
            ->setRequired('grid.price-package.overview-type.name.req');
        $container->addSelect('isRequired', 'grid.price-package.overview-type.is-required', Constant::DIAL_YES_NO);
    }

    public function onInlineEditDefaults(Container $container, PricePackageType $pricePackageTag): void
    {
        $container->setDefaults([
            'name' => $pricePackageTag->getName(),
            'isRequired' => intval($pricePackageTag->isRequired()),
        ]);
    }

    /**
     * @param int|string $id
     * @param ArrayHash<mixed> $values
     */
    public function onInlineEditSubmit($id, ArrayHash $values): void
    {
        $pricePackageTag = $this->facade->update(intval($id), $values->name, boolval($values->isRequired));

        $message = new SimpleTranslation('grid.price-package.overview-type.action.flash.inline-edit.success "%s"', [$pricePackageTag->getName()]);
        $this->onFlashmessage($message, Flash::SUCCESS);
    }
}
