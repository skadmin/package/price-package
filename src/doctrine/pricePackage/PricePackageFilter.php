<?php

declare(strict_types=1);

namespace Skadmin\PricePackage\Doctrine\PricePackage;

use SkadminUtils\DoctrineTraits\ACriteriaFilter;
use Doctrine\Common\Collections\Criteria;
use Nette\SmartObject;
use function count;

final class PricePackageFilter extends ACriteriaFilter
{
    use SmartObject;

    /** @var bool */
    private $onlyActive = true;

    /** @var array<PricePackageTag> */
    private $tags = [];

    /**
     * @param array<PricePackageTag> $tags
     */
    public function __construct(bool $onlyActive = true, array $tags = [])
    {
        $this->onlyActive = $onlyActive;
        $this->tags       = $tags;
    }

    public function isOnlyActive() : bool
    {
        return $this->onlyActive;
    }

    public function setOnlyActive(bool $onlyActive) : void
    {
        $this->onlyActive = $onlyActive;
    }

    /**
     * @return array<PricePackageTag>
     */
    public function getTags() : array
    {
        return $this->tags;
    }

    /**
     * @param array<PricePackageTag> $tags
     */
    public function setTags(array $tags) : void
    {
        $this->tags = $tags;
    }

    public function modifyCriteria(Criteria &$criteria, string $alias = 'a') : void
    {
        $expr = Criteria::expr();

        if ($this->onlyActive) {
            $criteria->andWhere($expr->eq($this->getEntityName($alias, 'isActive'), true));
        }

        if (count($this->getTags()) <= 0) {
            return;
        }

        $criteria->andWhere($expr->in('t.id', $this->getTags()));
    }
}
