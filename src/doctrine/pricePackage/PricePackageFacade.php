<?php

declare(strict_types=1);

namespace Skadmin\PricePackage\Doctrine\PricePackage;

use SkadminUtils\DoctrineTraits\Facade;
use App\Model\Doctrine\Traits;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Nettrine\ORM\EntityManagerDecorator;
use function intval;

final class PricePackageFacade extends Facade
{
    use Facade\Sequence;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = PricePackage::class;
    }

    public function getModelForGrid() : QueryBuilder
    {
        /** @var EntityRepository $repository */
        $repository = $this->em->getRepository($this->table);
        return $repository->createQueryBuilder('a')
            ->leftJoin('a.tags', 't')
            ->orderBy('a.sequence');
    }

    /**
     * @param array<PricePackageTag> $tags
     */
    public function create(string $name, string $content, int $price, bool $isActive, ?string $imagePreview, PricePackageType $type, array $tags) : PricePackage
    {
        return $this->update(null, $name, $content, $price, $isActive, $imagePreview, $type, $tags);
    }

    /**
     * @param array<PricePackageTag> $tags
     */
    public function update(?int $id, string $name, string $content, int $price, bool $isActive, ?string $imagePreview, PricePackageType $type, array $tags) : PricePackage
    {
        $pricePackage = $this->get($id);
        $pricePackage->update($name, $content, $price, $isActive, $imagePreview, $type, $tags);

        if (! $pricePackage->isLoaded()) {
            $pricePackage->setSequence($this->getValidSequence());
        }

        $this->em->persist($pricePackage);
        $this->em->flush();

        return $pricePackage;
    }

    public function get(?int $id = null) : PricePackage
    {
        if ($id === null) {
            return new PricePackage();
        }

        $pricePackage = parent::get($id);

        if ($pricePackage === null) {
            return new PricePackage();
        }

        return $pricePackage;
    }

    /**
     * @return PricePackage[]
     */
    public function getAll(bool $onlyActive = false) : array
    {
        $criteria = [];

        if ($onlyActive) {
            $criteria['isActive'] = true;
        }

        $orderBy = ['sequence' => 'ASC'];

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria, $orderBy);
    }

    public function getCount(?PricePackageFilter $filter = null) : int
    {
        $qb = $this->findPricePackageQb($filter);
        $qb->select('count(distinct a.id)');

        return intval($qb->getQuery()
            ->getSingleScalarResult());
    }

    /**
     * @return PricePackage[]
     */
    public function findPricePackage(?PricePackageFilter $filter = null, ?int $limit = null, ?int $offset = null) : array
    {
        return $this->findPricePackageQb($filter, $limit, $offset)
            ->getQuery()
            ->getResult();
    }

    private function findPricePackageQb(?PricePackageFilter $filter = null, ?int $limit = null, ?int $offset = null) : QueryBuilder
    {
        /** @var QueryBuilder $qb */
        $qb = $this->em
            ->getRepository($this->table)
            ->createQueryBuilder('a')
            ->leftJoin('a.tags', 't')
            ->distinct();

        $qb->setMaxResults($limit)
            ->setFirstResult($offset)
            ->orderBy('a.sequence', 'ASC');

        $criteria = Criteria::create();

        if ($filter !== null) {
            $filter->modifyCriteria($criteria);
        }

        $qb->addCriteria($criteria);

        return $qb;
    }
}
