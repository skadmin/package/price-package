<?php

declare(strict_types=1);

namespace Skadmin\PricePackage\Doctrine\PricePackage;

use SkadminUtils\DoctrineTraits\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class PricePackage
{
    use Entity\Id;
    use Entity\Name;
    use Entity\Content;
    use Entity\IsActive;
    use Entity\ImagePreview;
    use Entity\Sequence;

    #[ORM\Column]
    private int $price = 0;

    #[ORM\ManyToMany(targetEntity: PricePackageTag::class, inversedBy: 'pricePackages')]
    #[ORM\JoinTable(name: 'price_package_tag_rel')]
    #[ORM\OrderBy(['sequence' => 'ASC'])]
    /** @var ArrayCollection<PricePackageTag>|array<PricePackageTag> */
    private array|ArrayCollection|Collection $tags;

    #[ORM\ManyToOne(targetEntity: PricePackageType::class, inversedBy: 'pricePackageReservations')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private ?PricePackageType $type = null;

    /** @var PricePackageTag|null */
    private $firstTag = null;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
    }

    /**
     * @param array<PricePackageTag> $tags
     */
    public function update(string $name, string $content, int $price, bool $isActive, ?string $imagePreview, PricePackageType $type, array $tags): void
    {
        $this->name = $name;
        $this->content = $content;
        $this->price = $price;
        $this->isActive = $isActive;
        $this->type = $type;

        $this->tags = $tags;

        if ($imagePreview !== null && $imagePreview !== '') {
            $this->imagePreview = $imagePreview;
        }
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @return ArrayCollection<PricePackageTag>|array<PricePackageTag>
     */
    public function getTags(bool $onlyActive = false)
    {
        $criteria = Criteria::create();

        if ($onlyActive) {
            $criteria->where(Criteria::expr()->eq('isActive', true));
        }

        return $this->tags->matching($criteria);
    }

    public function getFirstTag(): ?PricePackageTag
    {
        if ($this->firstTag === null) {
            $firstTag = $this->tags->first();
            $this->firstTag = $firstTag ? $firstTag : null;
        }

        return $this->firstTag;
    }

    public function getType(): ?PricePackageType
    {
        return $this->type;
    }

    public function setType(PricePackageType $type): void
    {
        $this->type = $type;
    }

}
