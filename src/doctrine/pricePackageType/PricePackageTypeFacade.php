<?php

declare(strict_types=1);

namespace Skadmin\PricePackage\Doctrine\PricePackage;

use SkadminUtils\DoctrineTraits\Facade;
use App\Model\Doctrine\Traits;
use Nettrine\ORM\EntityManagerDecorator;

final class PricePackageTypeFacade extends Facade
{

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = PricePackageType::class;
    }

    public function create(string $name, bool $isRequired): PricePackageType
    {
        return $this->update(null, $name, $isRequired);
    }

    public function update(?int $id, string $name, bool $isRequired): PricePackageType
    {
        $pricePackageType = $this->get($id);
        $pricePackageType->update($name, $isRequired);

        $this->em->persist($pricePackageType);
        $this->em->flush();

        return $pricePackageType;
    }

    public function get(?int $id = null): PricePackageType
    {
        if ($id === null) {
            return new PricePackageType();
        }

        $pricePackageType = parent::get($id);

        if ($pricePackageType === null) {
            return new PricePackageType();
        }

        return $pricePackageType;
    }

    /**
     * @return PricePackageType[]
     */
    public function getAll(): array
    {
        $criteria = [];

        $orderBy = ['name' => 'ASC'];

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria, $orderBy);
    }

}
