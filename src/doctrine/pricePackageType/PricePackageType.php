<?php

declare(strict_types=1);

namespace Skadmin\PricePackage\Doctrine\PricePackage;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Nette\Utils\DateTime;
use SkadminUtils\DoctrineTraits\Entity;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class PricePackageType
{
    use Entity\Id;
    use Entity\Name;

    #[ORM\Column(options: ['default' => false])]
    private bool $isRequired = false;

    #[ORM\OneToMany(targetEntity: PricePackage::class, mappedBy: 'type')]
    #[ORM\OrderBy(['termFrom' => 'ASC'])]
    /** @var ArrayCollection|PricePackage[] */
    private array|ArrayCollection|Collection $pricePackage;

    public function __construct()
    {
        $this->pricePackage = new ArrayCollection();
    }

    public function update(string $name, bool $isRequired): void
    {
        $this->name = $name;
        $this->isRequired = $isRequired;
    }

    public function isRequired(): bool
    {
        return $this->isRequired;
    }

    public function setIsRequired(bool $isRequired): void
    {
        $this->isRequired = $isRequired;
    }

    /**
     * @return ArrayCollection|PricePackage[]
     */
    public function getPricePackage(bool $onlyValid = false, bool $onlyInFuture = false, ?int $limit = null)
    {
        $criteria = Criteria::create();

        if ($onlyValid) {
            $criteria->where(Criteria::expr()->eq('status', PricePackage::STATUS_CONFIRMED));
        }

        if ($onlyInFuture) {
            $criteria->andWhere(Criteria::expr()->gte('termFrom', new DateTime()));
        }

        $criteria->setMaxResults($limit);
        $criteria->orderBy(['termFrom' => 'ASC']);
        return $this->pricePackage->matching($criteria);
    }

    /**
     * Fire trigger every time on update
     *
     * @Doctrine\ORM\Mapping\PreUpdate
     * @Doctrine\ORM\Mapping\PrePersist
     */
    public function onPreUpdateWebalizeName(): void
    {
    }
}
