<?php

declare(strict_types=1);

namespace Skadmin\PricePackage\Doctrine\PricePackage;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use SkadminUtils\DoctrineTraits\Entity;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class PricePackageTag
{
    use Entity\Id;
    use Entity\IsActive;
    use Entity\WebalizeName;
    use Entity\Content;
    use Entity\Sequence;
    use Entity\ImagePreview;

    #[ORM\Column]
    private $color = '';

    #[ORM\ManyToMany(targetEntity: PricePackage::class, mappedBy: 'tags')]
    #[ORM\OrderBy(['sequence' => 'ASC'])]
    private array|ArrayCollection|Collection $pricePackages;

    public function __construct()
    {
        $this->pricePackages = new ArrayCollection();
    }

    public function update(string $name, string $content, string $color, bool $isActive, ?string $imagePreview) : void
    {
        $this->name    = $name;
        $this->content = $content;
        $this->color   = $color;

        $this->setIsActive($isActive);

        if ($imagePreview !== null && $imagePreview !== '') {
            $this->imagePreview = $imagePreview;
        }
    }

    public function getColor() : string
    {
        return $this->color === '' ? '#FFFFFF' : $this->color;
    }

    /**
     * @return ArrayCollection|PricePackage[]
     */
    public function getPricePackages(bool $onlyActive = false, ?int $limit = null)
    {
        $criteria = Criteria::create();

        if ($onlyActive) {
            $criteria->where(Criteria::expr()->eq('isActive', true));
        }

        $criteria->setMaxResults($limit);
        return $this->pricePackages->matching($criteria);
    }

    /**
     * Fire trigger every time on update
     *
     * @Doctrine\ORM\Mapping\PreUpdate
     * @Doctrine\ORM\Mapping\PrePersist
     */
    public function onPreUpdateWebalizeName() : void
    {
    }
}
