<?php

declare(strict_types=1);

namespace Skadmin\PricePackage\Doctrine\PricePackage;

use SkadminUtils\DoctrineTraits\Facade;
use App\Model\Doctrine\Traits;
use Nettrine\ORM\EntityManagerDecorator;

final class PricePackageTagFacade extends Facade
{
    use Facade\Webalize;
    use Facade\Sequence;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = PricePackageTag::class;
    }

    public function create(string $name, string $content, string $color, bool $isActive, ?string $imagePreview) : PricePackageTag
    {
        return $this->update(null, $name, $content, $color, $isActive, $imagePreview);
    }

    public function update(?int $id, string $name, string $content, string $color, bool $isActive, ?string $imagePreview) : PricePackageTag
    {
        $PricePackageTag = $this->get($id);
        $PricePackageTag->update($name, $content, $color, $isActive, $imagePreview);

        if (! $PricePackageTag->isLoaded()) {
            $PricePackageTag->setWebalize($this->getValidWebalize($name));
            $PricePackageTag->setSequence($this->getValidSequence());
        }

        $this->em->persist($PricePackageTag);
        $this->em->flush();

        return $PricePackageTag;
    }

    public function get(?int $id = null) : PricePackageTag
    {
        if ($id === null) {
            return new PricePackageTag();
        }

        $PricePackageTag = parent::get($id);

        if ($PricePackageTag === null) {
            return new PricePackageTag();
        }

        return $PricePackageTag;
    }

    /**
     * @return PricePackageTag[]
     */
    public function getAll(bool $onlyActive = false) : array
    {
        $criteria = [];

        if ($onlyActive) {
            $criteria['isActive'] = true;
        }

        $orderBy = ['sequence' => 'ASC'];

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria, $orderBy);
    }

    public function findByWebalize(string $webalize) : ?PricePackageTag
    {
        $criteria = ['webalize' => $webalize];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }
}
