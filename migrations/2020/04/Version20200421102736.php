<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Skadmin\PricePackage\BaseControl;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200421102736 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $data = [
            'base_control' => BaseControl::class,
            'webalize'     => 'price-package',
            'name'         => 'Price package',
            'is_active'    => 1,
        ];
        $this->addSql(
            'INSERT INTO core_package (base_control, webalize, name, is_active) VALUES (:base_control, :webalize, :name, :is_active)',
            $data
        );

        $resource = [
            'name'        => 'price-package',
            'title'       => 'role-resource.price-package.title',
            'description' => 'role-resource.price-package.description',
        ];
        $this->addSql('INSERT INTO resource (name, title, description) VALUES (:name, :title, :description)', $resource);
    }

    public function down(Schema $schema) : void
    {
    }
}
