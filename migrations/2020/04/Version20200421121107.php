<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200421121107 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $translations = [
            ['original' => 'price-package.overview', 'hash' => '3a67c13c685f2d97bce869b68a58f945', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Ceníkové balíčky', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.price-package.title', 'hash' => '7a9cc73f3f5135d2459ae408e53474cb', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Ceníkové balíčky', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.price-package.description', 'hash' => '60de49725531be4c4ce37b217ecf92ef', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Umožňuje spravovat ceníkové balíčky', 'plural1' => '', 'plural2' => ''],
            ['original' => 'price-package.overview.title', 'hash' => 'f7376817db28724279f81727f1c4d41f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Ceníkové balíčky|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.price-package.overview.is-active', 'hash' => 'b5bfd333bad549861a33a17e8aa06e8c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.price-package.overview.action.new', 'hash' => 'fcb5d59b07b2e1a7aba3ac03f964c62e', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založit balíček', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.price-package.overview.name', 'hash' => '9f8833d372ee114b04b4f8be67fa706f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.price-package.overview.price', 'hash' => '866ade88832e3b0ccc8d7e7c5a3100bd', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Cena', 'plural1' => '', 'plural2' => ''],
            ['original' => 'price-package.edit.title', 'hash' => '66ae1f76842d15b9c23b18d13419fa72', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založení ceníkového balíčku', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package.edit.name', 'hash' => '3f3871d0b8d97efaab9fa8951acba5de', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package.edit.name.req', 'hash' => '2bbcdb4c2c8ac4cda4aeb7fa182661a3', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím cenu', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package.edit.price', 'hash' => 'aad7e9759d16e446aef4c281dba4e03f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Cena', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package.edit.price.req', 'hash' => '5a9ae5d3085c431a04439df0ce0e5a73', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím cenu', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package.edit.image-preview', 'hash' => '79c519b916c67655ee201c4475a4bb5f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Náhled balíčku', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package.edit.image-preview.rule-image', 'hash' => '7ab423e659f09eb6b89950041bf28735', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vybraný soubor není platný obrázek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package.edit.content', 'hash' => '4fc91db6bde9a76d8f0a1f30ec45fb02', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Popis', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package.edit.is-active', 'hash' => '097f2e1ba010b268ef5bc1c07f07b047', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package.edit.send', 'hash' => '2382d6870edfeca3a5fb0e58f89e1667', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package.edit.send-back', 'hash' => 'cf9bca260361bd7f376be79f063300e8', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit a zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package.edit.back', 'hash' => '6597f55947bbeb20d3e61e7cf66d4cc0', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'price-package.edit.title - %s', 'hash' => '95bdc8dd6de4bc04d703b8e6f5f2ca65', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Editace ceníkového balíčku', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package.edit.flash.success.create', 'hash' => '9b2a94b9a8e35f3630dcafb077611b73', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Ceníkový balíček byl úspěšně založen.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package.edit.flash.success.update', 'hash' => 'd4090efe71c5d07c4225e464aed94807', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Ceníkový balíček byl úspěšně upraven.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.price-package.overview.action.edit', 'hash' => '9a19be229f820174cb18bbbb25b6342b', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.price-package.overview.action.flash.sort.success', 'hash' => 'ac06a472d980beeaa80f83c644b57c17', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Pořadí balíčků bylo upraveno.', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema) : void
    {
    }
}
