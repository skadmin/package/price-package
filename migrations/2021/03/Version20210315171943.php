<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210315171943 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $translations = [
            ['original' => 'grid.price-package.overview.tags', 'hash' => '93d279c5d9f318d3ca23fcc66138698d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Štítek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.price-package.overview.action.overview-tag', 'hash' => '359ba0db6a46df85f1062ee80f8eb278', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Přehled štítků', 'plural1' => '', 'plural2' => ''],
            ['original' => 'price-package.overview-tag.title', 'hash' => 'd1535d1cb049217c8f2e1f19124e62e0', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Štítky CB|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.price-package.overview-tag.action.overview', 'hash' => '86df847332efd6615b654f20ac42f2a9', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Přehled CB', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.price-package.overview-tag.action.new', 'hash' => 'fa77d62e772fb707d066c95d30904ff6', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založit štítek CB', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.price-package.overview-tag.name', 'hash' => 'b9c26546c823ecdc13fa67e89fa54973', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.price-package.overview-tag.color', 'hash' => '3910d4d40bf016cf2d8a343c19ae4c9d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Barva', 'plural1' => '', 'plural2' => ''],
            ['original' => 'price-package.edit-tag.title', 'hash' => '6e8dadac908b1ac0eb129a45d5b424cc', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založení štítku CB', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package.edit-tag.name', 'hash' => '4a02fde5b3245b8738088087f1298b4f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package.edit-tag.name.req', 'hash' => 'c938dfbd7509c3ef1a76042871673ab0', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package.edit-tag.color', 'hash' => 'eb40cd8be34e4da326d5ae65fb419d77', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Barva', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package.edit-tag.content', 'hash' => '14c99770bf2f2e8a4df1073557aa9792', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Popis', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package.edit-tag.is-active', 'hash' => 'cc3ea59545a7d036807f8075dbb41ce0', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package.edit-tag.send', 'hash' => 'ffbfa4bc43a966e1bd3c6dda0f881679', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package.edit-tag.send-back', 'hash' => '2bc970d05e39796aaffe73cbc192e71c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit a zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package.edit-tag.back', 'hash' => 'adc697494ada3797db31a0c0f21a8348', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'price-package.edit-tag.title - %s', 'hash' => 'fc30311f967b883e039a43e6c362e5cc', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Editace štítku CB', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package.edit-tag.flash.success.create', 'hash' => '1dbc42100d2b53c8001905f39e9fb812', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Štítek CB byl úspěšně založen.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package.edit-tag.flash.success.update', 'hash' => 'ffadb6076e52d8ad39a666d6d49f5cb3', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Štítek CB byl úspěšně upraven.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.price-package.overview-tag.action.edit', 'hash' => 'b32c2e76e37a85e7b32463de7b88e9aa', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.price-package.overview-tag.action.flash.sort.success', 'hash' => '276e44acde471561dfb9e176b8a753b2', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Pořadí štítků CB bylo upraveno.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package.edit.tags', 'hash' => '73d7f9f387752833ee7a0d482d47c8d0', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Štítky', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package.edit.tags.placeholder', 'hash' => 'b9f6a182eeb994a817b5c479c19d9a18', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Začněte psát pro vyhledávání...', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.price-package.edit.tags.no-result', 'hash' => 'd16180172d55be99bb7da64a0acf2061', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Štítek nebyl nalezen:', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
