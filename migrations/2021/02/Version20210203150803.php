<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Skadmin\PricePackage\BaseControl;
use function serialize;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210203150803 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE price_package_tag_rel (price_package_id INT NOT NULL, price_package_tag_id INT NOT NULL, INDEX IDX_B853BF2140C4A4FB (price_package_id), INDEX IDX_B853BF213F41A571 (price_package_tag_id), PRIMARY KEY(price_package_id, price_package_tag_id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE price_package_tag (id INT AUTO_INCREMENT NOT NULL, color VARCHAR(255) NOT NULL, is_active TINYINT(1) DEFAULT \'1\' NOT NULL, webalize VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, content LONGTEXT NOT NULL, sequence INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE price_package_tag_rel ADD CONSTRAINT FK_B853BF2140C4A4FB FOREIGN KEY (price_package_id) REFERENCES price_package (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE price_package_tag_rel ADD CONSTRAINT FK_B853BF213F41A571 FOREIGN KEY (price_package_tag_id) REFERENCES price_package_tag (id) ON DELETE CASCADE');

        $data = [
            'additional_privilege' => serialize(BaseControl::ADDITIONAL_PRIVILEGE),
            'name'                 => BaseControl::RESOURCE,
        ];
        $this->addSql('UPDATE resource SET additional_privilege = :additional_privilege WHERE name = :name', $data);
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE price_package_tag_rel DROP FOREIGN KEY FK_B853BF213F41A571');
        $this->addSql('DROP TABLE price_package_tag_rel');
        $this->addSql('DROP TABLE price_package_tag');
    }
}
